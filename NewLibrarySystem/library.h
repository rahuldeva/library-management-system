/*
 * library.h
 *
 *  Created on: Sep 28, 2022
 *      Author: Admin
 */

#ifndef LIBRARY_H_
#define LIBRARY_H_
#include<iostream>
#include<string>

#include"book.h"
#include"student.h"

class Library: public Book,Student{
private:
	Book book;
	Student student;
	vector<Book> booklist;
	vector<Student>studentlist;
public:
    Library()      =default;

    Library(int bookid,string book_name,int sroll,string sname);

    void set_book(Book book);

    void set_student(Student student);
    void add_book(string category);
    void delete_book(int choice);
    void search_book(int choice);
    void issue_book();
    void insert_data_into_issue_file(int bookid);
    void view_book_list(int choice);
    void view_book_list_by_entry();
    void edit_book_record();
    void view_student_list();
};



#endif /* LIBRARY_H_ */
