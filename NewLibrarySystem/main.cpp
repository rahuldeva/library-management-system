/*
 * main.cpp
 *
 *  Created on: Sep 28, 2022
 *      Author: Admin
 */

#include<iostream>
#include<vector>
#include"book.h"
#include"student.h"
#include"library.h"
using namespace std;

int menu_choice_for_books() {
	int choice;
	cout << "++++ SELECT CATEGORIES ++++ " << endl;
	cout << "1.Computer" << endl;
	cout << "2.Electronics" << endl;
	cout << "3.Electrical" << endl;
	cout << "4.Civil" << endl;
	cout << "5.Mechanical" << endl;
	cout << "6.Architecture" << endl;
	cout << "Back to menu" << endl;

	cout << "please enter choice :";
	cin >> choice;

	return choice;
}

int menu_choice() {
	int choice;

	cout << " ***** MAIN MENU ***** " << endl;
	cout << "1. Add Books" << endl;
	cout << "2. Delete Books" << endl;
	cout << "3. Search Books" << endl;
	cout << "4. Issue Books" << endl;
	cout << "5.View Book List" << endl;
	cout << "6. Edit Book Records" << endl;
	cout << "7.Close Application" << endl;

	cout << "please enter choice :";
	cin >> choice;

	return choice;
}

int remove_book() {
	int choice;

	cout << "1. Delete By Id" << endl;
	cout << "2. Delete By Name" << endl;

	cout << "Enter your choice :";
	cin >> choice;
	return choice;
}

int find_book() {
	int choice;

	cout << "1. Search By Id" << endl;
	cout << "2. Search By Name" << endl;

	cout << "Enter your choice :";
	cin >> choice;
	return choice;
}

int display_booklist() {
	int choice;

	cout << "1. Show Book List By Entry :" << endl;
	cout << "2. Show Book List By Id :" << endl;

	cout << "Enter your choice :";
	cin >> choice;
	return choice;
}

char choice_for_main_mainu_or_student_list(){
	char ch;
	cout<<"m. for main mainu :"<<endl;
	cout<<"s. for student information :"<<endl;
	cout<<"Enter Your Choice :";
	cin>>ch;
	return ch;
}
int main() {
	int choice, bookid, check = 0, studentid, check1 = 0;
	string category;
	char ch;

	Book book;

	Student student;
	vector<Student> studentlist;

	Library library;

	while ((choice = menu_choice()) != 0) {
		switch (choice) {
		case 1:
			while ((choice = menu_choice_for_books()) != 0) {
				switch (choice) {
				case 1:
					category = "Computer";
					break;
				case 2:
					category = "Electronics";
					break;
				case 3:
					category = "Electrical";
					break;
				case 4:
					category = "Civil";
					break;
				case 5:
					category = "Mechanical";
					break;
				case 6:
					category = "Architecture";
					break;
				case 7:
					cout << "Invalid Choice" << endl;
					exit(0);
					break;
				}
				break;

			}

			library.add_book(category);
			break;

		case 2:
			while ((choice = remove_book()) != 0) {
				switch (choice) {
				case 1:
					library.delete_book(choice);
					break;
				case 2:
					library.delete_book(choice);
					break;
				}
				break;
			}

			break;

		case 3:
			while ((choice = find_book()) != 0) {
				switch (choice) {
				case 1:
					library.search_book(choice);
					break;
				case 2:
					library.search_book(choice);
					break;
				}
				break;
			}

			break;

		case 4:
			library.issue_book();
			while((ch=choice_for_main_mainu_or_student_list())!=0){
				switch(ch){
				case 'm':
					break;
				case 's':
                    library.view_student_list();
					break;
				}
				break;
			}


			break;
		case 5:
			while ((choice = display_booklist()) != 0) {
				switch (choice) {
				case 1:
					library.view_book_list(choice);
					break;
				case 2:
					library.view_book_list(choice);
					break;
				}
				break;
			}
			break;


		case 6:
            library.edit_book_record();
		break;

		case 7:
			cout<<"end here............"<<endl;
			choice=0;
			break;

		default:
			cout<<"Invalid choice..."<<endl;
			break;
		}
	}

	return 0;
}

