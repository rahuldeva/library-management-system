#include <iostream>
#include<string>
#include<vector>
#include<fstream>
#include <sstream>
#include<iterator>
#include<string.h>
#include<windows.h>
#include<time.h>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<windows.h>
#include<time.h>
#include"student.h"
using namespace std;

Student::Student() {
	this->roll_number = 0;
	this->student_name = " ";
}

Student::Student(int roll_number, string student_name) {
	this->roll_number = roll_number;
	this->student_name = student_name;
}

int Student::get_roll_number() {
	return this->roll_number;
}

void Student::set_roll_number(int roll_number) {
	this->roll_number = roll_number;
}

string Student::get_student_name() {
	return this->student_name;
}

void Student::set_student_name(string student_name) {
	this->student_name = student_name;
}

int Student::add_student() {
	cout << "student roll number :" << endl;
	cin >> this->roll_number;

	cout << "student name :" << endl;
	cin >> this->student_name;

	return this->roll_number;
}

void Student::insert_data_into_student_file() {
	ofstream file;
	file.open("students.txt", ios::app);
	file << this->get_roll_number() << " " << this->get_student_name() << endl;
	file.close();
}

int Student::read_student_data_from_file(vector<Student> &studentlist,
		int roll_number) {
	ifstream fp;
	int flag = 0;
	fp.open("students.txt");
	string line;
	if (!fp) {
		cerr << "Failed To open File" << endl;

	}

	while (getline(fp, line)) {
		unsigned i;

		stringstream str(line);
		string token[2];
		for (i = 0; i < 2; i++) {
			getline(str, token[i], ' ');
		}
		Student obj(stoi(token[0]), token[1]);
		studentlist.push_back(obj);
	}
	for (int i = 0; i < studentlist.size(); i++) {
		if (studentlist[i].get_roll_number() == roll_number) {
			cout << "Id Already Exit " << endl;
			flag = 1;
			break;
		}
	}
	fp.close();
	return flag;
}

void Student::sort_student_list_by_id(){
	vector<Student>studentlist;
		ifstream fp;
		int flag = 0;
		fp.open("students.txt");
		string line;
		if (!fp) {
			cerr << "Failed To open File" << endl;
		}

		while (getline(fp, line)) {
			unsigned i;

			stringstream str(line);
			string token[2];
			for (i = 0; i < 2; i++) {
				getline(str, token[i], ' ');
			}
			Student obj(stoi(token[0]), token[1]);
			studentlist.push_back(obj);
		}

		fp.close();
		sort(studentlist.begin(), studentlist.end(), [](auto &lhs, auto &rhs) {
			return lhs.get_roll_number()   < rhs.get_roll_number();
		});

		cout
				<< "\n\n================================================================================";
		for (int i = 0; i < studentlist.size(); i++) {
			cout << "\n\n" << studentlist[i].get_roll_number() << " "
					<< studentlist[i].get_student_name() <<  endl;
		}

		cout
				<< "\n\n================================================================================\n\n";

}
