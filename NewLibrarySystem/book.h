/*
 * book.h
 *
 *  Created on: Sep 28, 2022
 *      Author: Admin
 */

#ifndef BOOK_H_
#define BOOK_H_
#include<iostream>
#include<string>
using namespace std;
class Book{
private:
    int book_id;
    string category;
    string book_name;
    string author_name;
    int quantity;
    int price;
    int rack_no;

public:
    Book();
    Book(int book_id, string category, string book_name, string author_name, int quantity,int price, int rack_no);
    int get_book_id();
    void set_book_id(int book_id);
    string get_category();
    void set_category(string category);
    string get_book_name();
    void set_book_name(string book_name);
    string get_author_name();
    void set_author_name(string author_name);
    int get_quantity();
    void set_quantity(int quantity);
    int get_price();
    void set_price(int price);
    int get_rack_no();
    void set_rack_no(int rack_no);
    int add_book(string category);
    void delete_book(int choice);
    void search_book(int choice);
    void insert_data_into_book_file();
    int read_book_data_from_file(vector<Book> &booklist,int bookid);
    void sort_booklist_by_id();
    void edit_book_records_on_file();
    int book_category();
};



#endif /* BOOK_H_ */
