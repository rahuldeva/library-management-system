/*
 * student.h
 *
 *  Created on: Sep 28, 2022
 *      Author: Admin
 */

#ifndef STUDENT_H_
#define STUDENT_H_
#include<iostream>
#include<string>
#include<vector>
using namespace std;
class Student {
private:
	int roll_number;
	string student_name;
public:
	Student();
	Student(int roll_number, string student_name);

	int get_roll_number();
	void set_roll_number(int roll_number);

	string get_student_name();
	void set_student_name(string student_name);

	int add_student();
	void insert_data_into_student_file();
	int read_student_data_from_file(vector<Student> &studentlist,
			int roll_number);

	void sort_student_list_by_id();
};

#endif /* STUDENT_H_ */
