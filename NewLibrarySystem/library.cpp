/*
 * library.cpp
 *
 *  Created on: Sep 28, 2022
 *      Author: Admin
 */
#include <iostream>
#include<string>
#include<vector>
#include<fstream>
#include <sstream>
#include<iterator>
#include<string.h>
#include<windows.h>
#include<time.h>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<windows.h>
#include"library.h"
using namespace std;

void Library::set_book(Book book) {
	this->book = book;
}

void Library::set_student(Student student) {
	this->student = student;
}

Library::Library(int bookid, string book_name, int sroll, string sname) {
	this->book.set_book_id(bookid);
	this->book.set_book_name(book_name);
	this->student.set_roll_number(sroll);
	this->student.set_student_name(sname);
}
void Library::issue_book() {

	int flag = 0, bookid;
	cout << "bookid :";
	cin >> bookid;
	flag = this->book.read_book_data_from_file(this->booklist, bookid);
	if (flag == 1)
		this->insert_data_into_issue_file(bookid);
	else
		cout << "Book Id not found..." << endl;

}

void Library::insert_data_into_issue_file(int bookid) {
	int studentid, check;
	ofstream file;
	string bname;
	studentid = this->student.add_student();
	check = this->student.read_student_data_from_file(this->studentlist,
			studentid);
	if (check == 0) {
		this->student.insert_data_into_student_file();
	} else {
		cout << "student is already exist" << endl;
	}
	file.open("issue.txt", ios::app);
	for (int i = 0; i < this->booklist.size(); i++) {
		if (this->booklist[i].get_book_id() == bookid) {
			bookid = booklist[i].get_book_id();
			bname = booklist[i].get_book_name();
			break;
		}
	}

	file << bookid << " " << bname << " " << this->student.get_roll_number()
			<< " " << this->student.get_student_name() << endl;

	file.close();
}

void Library::add_book(string category) {
	int bookid, check = 0;
	bookid = this->book.add_book(category);
	check = this->book.read_book_data_from_file(this->booklist, bookid);
	if (check == 0) {
		this->book.insert_data_into_book_file();
	}

}

void Library::view_book_list_by_entry() {

	ifstream fp;
	int flag = 0;
	fp.open("issue.txt");
	string line;
	if (!fp) {
		cerr << "Failed To open File" << endl;
	}

	while (getline(fp, line)) {
		unsigned i;

		stringstream str(line);
		string token[4];
		for (i = 0; i < 4; i++) {
			getline(str, token[i], ' ');
		}
		Library obj(stoi(token[0]), token[1], stoi(token[2]), token[3]);
		cout<< "\n\n================================================================================\n\n";
		cout << obj.book.get_book_id() << " " << obj.book.get_book_name() << " "
				<< obj.student.get_roll_number() << " "
				<< obj.student.get_student_name()<<endl;
	}
	cout<< "\n\n================================================================================\n\n";
	fp.close();

}

void Library::view_book_list(int choice) {
	while (choice != 0) {
		switch (choice) {
		case 1:
			view_book_list_by_entry();
			break;
		case 2:
			this->book.sort_booklist_by_id();
			break;
		}
		break;
	}
}

void Library::edit_book_record(){
this->book.edit_book_records_on_file();
}
void Library::delete_book(int choice) {
	this->book.delete_book(choice);
}

void Library::search_book(int choice) {
	this->book.search_book(choice);
}

void Library::view_student_list(){
	this->student.sort_student_list_by_id();
}
