/*
 * book.cpp
 *
 *  Created on: Sep 28, 2022
 *      Author: Admin
 */

#include <iostream>
#include<string>
#include<vector>
#include<fstream>
#include <sstream>
#include<iterator>
#include<string.h>
#include<windows.h>
#include<time.h>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<windows.h>
#include<time.h>
#include"book.h"
using namespace std;

Book::Book() {
	this->book_id = 0;
	this->category = " ";
	this->book_name = " ";
	this->author_name = " ";
	this->quantity = 0;
	this->price = 0;
	this->rack_no = 0;
}

Book::Book(int book_id, string category, string book_name, string author_name,
		int quantity, int price, int rack_no) {

	this->book_id = book_id;
	this->category = category;
	this->book_name = book_name;
	this->author_name = author_name;
	this->quantity = quantity;
	this->price = price;
	this->rack_no = rack_no;
}

int Book::get_book_id() {
	return this->book_id;
}

void Book::set_book_id(int book_id) {
	this->book_id = book_id;
}

string Book::get_category() {
	return this->category;
}

void Book::set_category(string category) {
	this->category = category;
}

string Book::get_book_name() {
	return this->book_name;
}

void Book::set_book_name(string book_name) {
	this->book_name = book_name;
}

string Book::get_author_name() {
	return this->author_name;
}

void Book::set_author_name(string author_name) {
	this->author_name = author_name;
}

int Book::get_quantity() {
	return this->quantity;
}

void Book::set_quantity(int quantity) {
	this->quantity = quantity;
}

int Book::get_price() {
	return this->price;
}

void Book::set_price(int price) {
	this->price = price;
}
int Book::get_rack_no() {
	return this->rack_no;
}

void Book::set_rack_no(int rack_no) {
	this->rack_no = rack_no;
}

int Book::add_book(string category) {

	cout << "book id:";
	cin >> this->book_id;

	this->set_category(category);
	cout << "book name:";
	cin >> this->book_name;

	cout << "auther :";
	cin >> this->author_name;

	cout << "price :";
	cin >> this->price;

	cout << "quantity :";
	cin >> this->quantity;

	cout << "rack_no:";
	cin >> this->rack_no;

	return this->book_id;
}

void Book::delete_book(int choice) {
	char temp_search[500], temp_search1[500];
	char temp_str[500];
	fstream fin("books.txt", ios::in);
	if (choice == 1) {
		cout << "Enter Id :";
		cin.ignore();
		cin.getline(temp_search, 500);

		if (fin.getline(temp_search1, 500)) {
			fin.seekg(0, ios::beg);
			char q;
			cout << "\nAre you sure you want to delete the record (Y/N)? ";
			cin >> q;
			if (q == 'Y' | q == 'y') {
				fstream fout("temp.txt", ios::out | ios::app);
				fout.seekg(0, ios::end);
				while (fin.getline(temp_str, 500)) {

					if (temp_str[0] != temp_search[0]) {
						fout << temp_str << endl;

					}
				}
				fin.close();
				fout.close();
				remove("books.txt");
				rename("temp.txt", "books.txt");
				cout << "\n....Successfully Deleted...." << endl;
			}
		} else
			cout << "\nNo books are found to be deleted" << endl;
	} else if (choice == 2) {
		cout << "\nEnter the book name : ";
		cin.ignore();
		cin.getline(temp_search, 500);
		if (fin.getline(temp_search1, 500)) {
			fin.seekg(0, ios::beg);
			char q;
			cout << "\nAre you sure you want to delete the record (Y/N)? ";
			cin >> q;
			if (q == 'Y' | q == 'y') {
				fstream fout("temp.txt", ios::out | ios::app);
				fout.seekg(0, ios::end);

				while (fin.getline(temp_str, 500)) {

					if (strstr(temp_str, temp_search) == NULL) {
						fout << temp_str << endl;
					}
				}
				fin.close();
				fout.close();
				remove("books.txt");
				rename("temp.txt", "books.txt");
				cout << "\n....Successfully Deleted...." << endl;
			}
		} else
			cout << "\nNo books are found to be deleted";
	}

}

void Book::search_book(int choice) {
	char str_line2[500], str_line5[500];
	fstream file("books.txt", ios::in);
	if (choice == 1) {

		cout << "\nEnter book id : ";
		char bookid[10];
		cin.ignore();
		cin.getline(bookid, 50);
		if (file.getline(str_line2, 500)) {
			file.seekg(0, ios::beg);
			int s = 1;
			cout
					<< "\n\n================================================================================";
			while (file.getline(str_line2, 500)) {
				if (str_line2[0] == bookid[0]) {
					cout << "\n\n" << str_line2;
					s = 0;
				}
			}
			if (s == 1) {
				cout << "\nBook not found";
			}
			cout
					<< "\n\n================================================================================\n\n";
		} else
			cout << "\nBook not found" << endl;
		file.close();
	} else if (choice == 2) {

		cout << "\nEnter book name : ";
		char bookname[10];
		cin.ignore();
		cin.getline(bookname, 50);
		if (file.getline(str_line2, 500)) {
			file.seekg(0, ios::beg);
			int s = 1;
			cout
					<< "\n\n================================================================================";
			while (file.getline(str_line2, 500)) {
				if (strstr(str_line2, bookname) != NULL) {
					cout << "\n\n" << str_line2;
					s = 0;
				}
			}
			if (s == 1) {
				cout << "\nBook not found" << endl;
			}
			cout
					<< "\n\n================================================================================\n\n";
		} else
			cout << "\nBook not found" << endl;

		file.close();

	}
}

void Book::insert_data_into_book_file() {

	ofstream file;
	file.open("books.txt", ios::app);

	file << this->get_book_id() << " " << this->get_category() << " "
			<< this->get_book_name() << " " << this->get_author_name() << " "
			<< this->get_price() << " " << this->get_quantity() << " "
			<< this->get_rack_no() << "\n";
	file.close();
}

int Book::read_book_data_from_file(vector<Book> &booklist, int bookid) {
	ifstream fp;
	int flag = 0;
	fp.open("books.txt");
	string line;
	if (!fp) {
		cerr << "Failed To open File" << endl;

	}

	while (getline(fp, line)) {
		unsigned i;

		stringstream str(line);
		string token[7];
		for (i = 0; i < 7; i++) {
			getline(str, token[i], ' ');
		}
		Book obj(stoi(token[0]), token[1], token[2], token[3], stoi(token[4]),
				stoi(token[5]), stoi(token[6]));
		booklist.push_back(obj);
	}
	for (int i = 0; i < booklist.size(); i++) {
		if (booklist[i].get_book_id() == bookid) {
			cout << "Id Already Exit " << endl;
			flag = 1;
			break;
		}
	}
	fp.close();
	return flag;
}

void Book::sort_booklist_by_id() {
	vector<Book> booklist;
	ifstream fp;
	int flag = 0;
	fp.open("books.txt");
	string line;
	if (!fp) {
		cerr << "Failed To open File" << endl;
	}

	while (getline(fp, line)) {
		unsigned i;

		stringstream str(line);
		string token[7];
		for (i = 0; i < 7; i++) {
			getline(str, token[i], ' ');
		}
		Book obj(stoi(token[0]), token[1], token[2], token[3], stoi(token[4]),
				stoi(token[5]), stoi(token[6]));
		booklist.push_back(obj);
	}

	fp.close();
	sort(booklist.begin(), booklist.end(), [](auto &lhs, auto &rhs) {
		return lhs.get_book_id() < rhs.get_book_id();
	});

	cout
			<< "\n\n================================================================================";
	for (int i = 0; i < booklist.size(); i++) {
		cout << "\n\n" << booklist[i].get_book_id() << " "
				<< booklist[i].get_category() << " "
				<< booklist[i].get_book_name() << " " << booklist[i].get_price()
				<< " " << booklist[i].get_quantity() << " "
				<< booklist[i].get_rack_no() << endl;
	}

	cout
			<< "\n\n================================================================================\n\n";

}

int Book::book_category() {
	int choice;
	cout << "++++ SELECT CATEGORIES ++++ " << endl;
	cout << "1.Computer" << endl;
	cout << "2.Electronics" << endl;
	cout << "3.Electrical" << endl;
	cout << "4.Civil" << endl;
	cout << "5.Mechanical" << endl;
	cout << "6.Architecture" << endl;
	cout << "Back to menu" << endl;

	cout << "please enter choice :";
	cin >> choice;

	return choice;
}
void Book::edit_book_records_on_file() {
	int bookid, found = 0, choice;
	char temp_search[500], temp_search1[500];
	char temp_str[500];
	int ref;
	string category, bookname, author;
	cout << "Enter Id :";
	cin.ignore();
	cin.getline(temp_search,500);
	bookid = stoi(temp_search);
	fstream fin("books.txt", ios::in);

	while ((choice = this->book_category()) != 0) {
		switch (choice) {
		case 1:
			category = "Computer";
			break;
		case 2:
			category = "Electronics";
			break;
		case 3:
			category = "Electrical";
			break;
		case 4:
			category = "Civil";
			break;
		case 5:
			category = "Mechanical";
			break;
		case 6:
			category = "Architecture";
			break;
		case 7:
			cout << "Invalid Choice" << endl;
			exit(0);
			break;

		}
		break;
	}

	if (fin.getline(temp_search1, 500)) {

		fin.seekg(0, ios::beg);
		char q;
		cout << "\nAre you sure you want to edit the record (Y/N)? ";
		cin >> q;
		if (q == 'Y' | q == 'y') {
			fstream fout("temp.txt", ios::out | ios::app);
			fout.seekg(0, ios::end);
			while (fin.getline(temp_str, 500)) {

				if (temp_str[0] != temp_search[0]) {
					fout << temp_str << endl;
				}else{
					ref=temp_str[0];
				}
			}
			fin.close();
			fout.close();
			remove("books.txt");
			rename("temp.txt", "books.txt");
		}
	}
	else {
		cout << "\nNo books are found to be edited" << endl;
	}


	if(ref==temp_search[0]){
	this->add_book(category);
	fstream fout("books.txt", ios::out | ios::app);
	fout << this->get_book_id() << " " << this->get_category() << " "
			<< this->get_book_name() << " " << this->get_author_name() << " "
			<< this->get_price() << " " << this->get_quantity() << " "
			<< this->get_rack_no() << endl;
	fout.close();
	}else{
		cout<<"No Books Found to be edited ......"<<endl;
	}

}
